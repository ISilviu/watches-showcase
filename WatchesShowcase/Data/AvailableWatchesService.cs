﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace WatchesShowcase.Data
{
    public class AvailableWatchesService
    {
        public async Task<List<Watch>> GetAllWatchesAsync(string path, Func<Watch, bool> predicate = null)
        {
            var watches = new List<Watch>();
            path = Path.GetFullPath(path);

            using (var fileStream = File.OpenRead(path))
            {
                watches = await JsonSerializer.DeserializeAsync<List<Watch>>(fileStream);
                if(predicate != null)
                    watches = watches.AsParallel().Where(predicate).ToList();
            }
            return watches;
        }
    }
}
