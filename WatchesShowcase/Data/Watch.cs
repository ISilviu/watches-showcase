﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WatchesShowcase.Data
{
    public class Watch
    {
        public string Producer { get; set; }
        public string ModelCode { get; set; }
        public string ImagePath { get; set; }

        public Watch()
        { }

        public Watch(string producer, string modelCode, string imagePath)
        {
            Producer = producer;
            ModelCode = modelCode;
            ImagePath = imagePath;
        }
    }
}
